
#include <iostream>

#include "Polygon.h"
#include "Vertex.h"

#include "SortedVector.h"

int main(int argc, char* argv[])
{
	/*Vertex varr[] = { Vertex(0,0), Vertex(6,0), 
		Vertex(6.56,6), Vertex(0,6) };

	Polygon pol(varr, 4);
	Polygon palle;

	std::cout << "num: " << pol.numVertices() << std::endl;
	std::cout << "yta: " << pol.area() << std::endl;
	std::cout << "minx: " << pol.minx() << std::endl;
	std::cout << "maxx: " << pol.maxx() << std::endl;
	std::cout << "miny: " << pol.miny() << std::endl;
	std::cout << "maxy: " << pol.maxy() << std::endl;

	pol.printVertices(std::cout);

	pol.add(Vertex(-1, 3));
	std::cout << "num: " << pol.numVertices() << std::endl;
	std::cout << "yta: " << pol.area() << std::endl;
	std::cout << "minx: " << pol.minx() << std::endl;

	Polygon pol1;
 	pol1.add(Vertex(0, 0)); 
	pol1.add(Vertex(3, 3));
	pol1.add(Vertex(3, 0));

	std::cout << "Triangelyta: " << pol1.area() << std::endl;

	system("Pause");*/

	/////////////////////

	SortedVector<Polygon, 2> polygons;
	SortedVector<int, 2> ints;

	ints.print(std::cout);

	ints.add(3);
	ints.add(1);
	ints.add(6);

	ints.print(std::cout);

	Vertex varr[10];
	varr[0] = Vertex(0, 0);
	varr[1] = Vertex(10, 0);
	varr[2] = Vertex(5, 2);
	varr[3] = Vertex(5, 5);
	polygons.add(Polygon(varr, 4));

	std::cout << polygons.GetElement(0).area() << std::endl;

	varr[0] = Vertex(0, 0);
	varr[1] = Vertex(25, 8);
	varr[2] = Vertex(10, 23);
	//polygons.add(Polygon(varr, 3));
	std::cout << (Polygon(varr, 3)).area() << std::endl;
	varr[0] = Vertex(0, 0);
	varr[1] = Vertex(5, 0);
	varr[2] = Vertex(5, 3);
	varr[3] = Vertex(4, 8);
	varr[4] = Vertex(2, 10);
	//polygons.add(Polygon(varr, 5));
	std::cout << (Polygon(varr, 5)).area() << std::endl;
	//polygons.print(std::cout);
	ints.print(std::cout);

	return 1;
}