#pragma once
#include <iostream>
#include <fstream>

template <class T, int size> class SortedVector
{
	private:
		T vec[size];
		unsigned int m_nrOfElements;
		unsigned int m_MAX;
	public:
		SortedVector();
		~SortedVector();
		bool add(const T& p_Vector);
		void print(std::ostream &os);
		T GetElement(int pos) { return vec[pos]; }
	private:
		void sortVector();
};

template<class T, int size>
inline SortedVector<T, size>::SortedVector()
{
	for (int i = 0; i < size; i++)
	{
		vec[i] = T();
	}
	m_nrOfElements = 0;
	m_MAX = size;
}

template<class T, int size>
inline SortedVector<T, size>::~SortedVector()
{
	delete[] vec;
}

template<class T, int size>
inline bool SortedVector<T, size>::add(const T & p_Element)
{	
	if (m_nrOfElements == m_MAX)
	{
		printf("Vector full\n");
		return false;
	}

	if (m_nrOfElements == 0)
	{
		vec[m_nrOfElements] = p_Element;
		++m_nrOfElements;
		return true;
	}

	vec[m_nrOfElements] = p_Element;
	sortVector();
	++m_nrOfElements;
	return true;
}

template<class T, int size>
inline void SortedVector<T, size>::print(std::ostream & os)
{
	for (int pos = 0; pos != size; ++pos)
	{
		os << '[' << vec[pos] << "], ";
	}
	os << "\n";
}

template<class T, int size>
inline void SortedVector<T, size>::sortVector()
{
	for (int i = 0; i < m_MAX - 1; i++)
	{
		if (vec[i] > vec[i + 1])
		{
			T storeEl = vec[i];
			vec[i] = vec[i + 1];
			vec[i + 1] = storeEl;
		}	
	}
}
