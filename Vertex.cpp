#include "Vertex.h"

Vertex::Vertex()
{
	m_PositionX = 0;
	m_PositionY = 0;
}

Vertex::Vertex(float p_PositionX, float p_PositionY)
{
    m_PositionX = p_PositionX;
    m_PositionY = p_PositionY;
}

Vertex::~Vertex()
{
}
