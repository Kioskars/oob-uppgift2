#ifndef VERTEX_H
#define VERTEX_H


class Vertex
{

     private:
		float m_PositionX;
		float m_PositionY;

    public:
		Vertex();
        Vertex(float p_PositionX, float p_PositionY);
        ~Vertex();

		float GetPositionX() { return m_PositionX; }
        void SetPositionX(float p_PositionX) { m_PositionX = p_PositionX; }
		float GetPositionY() { return m_PositionY; }
        void SetPositionY(float p_PositionY) { m_PositionY = p_PositionY; }

    private:
		

};

#endif // VERTEX_H
