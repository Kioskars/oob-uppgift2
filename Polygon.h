#ifndef POLYGON_H
#define POLYGON_H

#include "Vertex.h"

#include <iostream>
#include <iomanip>

class Polygon
{

    private:
		Vertex* m_VertexArray;
		unsigned int m_iNrOfVertices;
    public:
		Polygon();
        Polygon(Vertex p_VertexArray[], unsigned int p_VertexArraySize);
		Polygon(const Polygon & polRef);

		virtual ~Polygon();
		
		const Polygon& Polygon::operator=(const Polygon& pol);
		const bool Polygon::operator<(const Polygon& pol);
		const bool Polygon::operator>(const Polygon& pol);
		const bool Polygon::operator==(const Polygon& pol);
		const bool Polygon::operator!=(const Polygon& pol);


        void add(Vertex p_Vertex);
		float area() const;
       
        float minx();	
		float maxx();
		float miny();
		float maxy();

		void printVertices(std::ostream & os);

		unsigned int numVertices();
    private:	
		
};

#endif // POLYGON_H
