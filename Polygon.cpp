#include "Polygon.h"

Polygon::Polygon() : m_iNrOfVertices(0), m_VertexArray(0)
{
	std::cout << "Hello default Polygon " << m_iNrOfVertices << std::endl;
}

Polygon::Polygon(Vertex p_VertexArray[], unsigned int p_iNrOfVertices) : m_iNrOfVertices(p_iNrOfVertices)
{
	if (m_iNrOfVertices > 0)
	{
		m_VertexArray = new Vertex[m_iNrOfVertices];
		m_VertexArray = p_VertexArray;
	}
	else
	{
		m_VertexArray = 0;
	}
	std::cout << "Hello �verlagrad " << m_iNrOfVertices << std::endl;
	printVertices(std::cout);
}

Polygon::Polygon(const Polygon & polRef) : m_iNrOfVertices(polRef.m_iNrOfVertices)
{
	std::cout << "Hello kopiering: Ny l�ngd: " << m_iNrOfVertices << std::endl;

	if (m_iNrOfVertices > 0)
	{
		m_VertexArray = new Vertex[m_iNrOfVertices];
		for (int i = 0; i < m_iNrOfVertices; i++)
			m_VertexArray[i] = polRef.m_VertexArray[i];
	}
	else
	{
		m_VertexArray = 0;
	}
	printVertices(std::cout);
}

Polygon::~Polygon()
{
	delete[] m_VertexArray;
	m_VertexArray = nullptr;
}

const Polygon & Polygon::operator=(const Polygon & pol)
{
	if (this != &pol)
	{
		m_iNrOfVertices = pol.m_iNrOfVertices;
		delete[] m_VertexArray;
		m_VertexArray = new Vertex[m_iNrOfVertices];

		for (int i = 0; i < m_iNrOfVertices; i++)
			m_VertexArray[i] = pol.m_VertexArray[i];
	}
	return *this;
}

const bool Polygon::operator<(const Polygon & pol)
{
	return area() < pol.area();
}

const bool Polygon::operator>(const Polygon & pol)
{
	return area() > pol.area();
}

const bool Polygon::operator==(const Polygon & pol)
{
	return area() == pol.area();
}

const bool Polygon::operator!=(const Polygon & pol)
{
	return !(*this == pol);
}



void Polygon::add(Vertex p_Vertex)
{
	if (m_VertexArray == 0)
		m_VertexArray = new Vertex;
	m_VertexArray[m_iNrOfVertices] = p_Vertex;
	m_iNrOfVertices++;
}

float Polygon::area() const
{
	float area = 0;
	float xVertexCalc = 0;
	for (unsigned int i = 0; i < m_iNrOfVertices; i++)
	{
		if(i == 0) //First position calculation
		{ 
			xVertexCalc = m_VertexArray[i].GetPositionX() * (m_VertexArray[i + 1].GetPositionY() - m_VertexArray[m_iNrOfVertices - 1].GetPositionY());
		}
		else if (i == m_iNrOfVertices - 1) //Last position Calculation
		{
			xVertexCalc = m_VertexArray[i].GetPositionX() * (m_VertexArray[0].GetPositionY() - m_VertexArray[i - 1].GetPositionY());
		}
		else //Calculations on vertices in between
		{
			xVertexCalc = m_VertexArray[i].GetPositionX() * (m_VertexArray[i + 1].GetPositionY() - m_VertexArray[i - 1].GetPositionY());
		}
		area += xVertexCalc;
	}
	
	return area / 2;
}


float Polygon::minx()
{
	float minx = m_VertexArray[0].GetPositionX();
	for (unsigned int i = 0; i < m_iNrOfVertices; i++)
	{
		minx = minx < m_VertexArray[i].GetPositionX() ? minx : m_VertexArray[i].GetPositionX();
	}
	return minx;
}

float Polygon::maxx()
{
	float maxx = m_VertexArray[0].GetPositionX();

	for (unsigned int i = 0; i < m_iNrOfVertices; i++)
	{
		if (maxx < m_VertexArray[i].GetPositionX())
			maxx = m_VertexArray[i].GetPositionX();
	}
	return maxx;
}

float Polygon::miny()
{
	float miny = m_VertexArray[0].GetPositionY();
	for (unsigned int i = 0; i < m_iNrOfVertices; i++)
	{
		miny = miny < m_VertexArray[i].GetPositionY() ? miny : m_VertexArray[i].GetPositionY();
	}
	return miny;
}

float Polygon::maxy()
{
	float maxy = m_VertexArray[0].GetPositionY();

	for (unsigned int i = 0; i < m_iNrOfVertices; i++)
	{
		if (maxy < m_VertexArray[i].GetPositionY())
			maxy = m_VertexArray[i].GetPositionY();
	}
	return maxy;
}

void Polygon::printVertices(std::ostream & os)
{
	for (int i = 0; i < m_iNrOfVertices; i++)
	{
		os << std::left << "Vertex " << i << ": "
			<< "X:" << std::setw(6) << m_VertexArray[i].GetPositionX() << " "
			<< std::setw(0) << "Y:" << std::setw(6) << m_VertexArray[i].GetPositionY() << std::endl;
	}
}

unsigned int Polygon::numVertices()
{
	return m_iNrOfVertices;
}




